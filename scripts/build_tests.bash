#!/bin/bash
set -o pipefail -o errexit -o nounset
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Default Debug build, unless we're building for CI.
BUILD_TYPE=Debug
if [[ -n "${CI+x}" ]]; then
    BUILD_TYPE=Release
fi
BUILD_DIR="${1:-"${SCRIPT_DIR}/../build/test"}"

mkdir -p "${BUILD_DIR}"
cd "${BUILD_DIR}"
cmake "${SCRIPT_DIR}"/.. -DCMAKE_BUILD_TYPE="${BUILD_TYPE}" -DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++
cmake --build . --parallel "$(nproc)"
