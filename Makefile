CXX = g++
# Extra compile flags, useful for conditionally including debug information.
# If you change the arguments, run a clean build
EXTRA_CXX_FLAGS ?= 
CXXFLAGS := ${INC_FLAGS} -MMD -MP -Wall -std=c++14 -DNDEBUG ${EXTRA_CXX_FLAGS}
LINKER_FLAGS ?=

BUILD_DIR ?= ./build
SRC_DIRS ?= src/
INC_DIRS := $(SRC_DIRS)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

SRCS := $(shell find $(SRC_DIRS) -type f -iregex ".*\.\(cpp\|cc\)")
HEADERS := $(shell find $(SRC_DIRS) -type f -iregex ".*\.\(hpp\|h\)")
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)
# List of all directories that we'll need in the build directory.
BUILD_DIRS := $(sort $(dir ${OBJS}))

# This file should have the following variables:
# TARGET_EXEC: name of the target executable
SUBMISSION_INFO_FILE := submission_info.mk
include $(SUBMISSION_INFO_FILE)

# Object files to target executable
$(TARGET_EXEC): $(OBJS)
	$(CXX) ${LINKER_FLAGS} $^ -o $@

$(BUILD_DIRS):
	$(MKDIR_P) $@


# If we want to handle multiple extensions for source files (.cc, .cpp),
# then things get a little ugly. I found this solution on https://stackoverflow.com/questions/27952558/make-pattern-rule-matching-multiple-extensions.
define COMPILE
$(CXX) $(CXXFLAGS) -c $< -o $@
endef

# C++ source to object files
$(BUILD_DIR)/%.cpp.o: %.cpp | $(BUILD_DIRS)
	$(COMPILE)

$(BUILD_DIR)/%.cc.o: %.cc | $(BUILD_DIRS)
	$(COMPILE)

clean:
	$(RM) -r $(BUILD_DIR)

ZIP_NAME := $(TARGET_EXEC).zip

# We'll need the absolute path to the makefile, since we'll most likely
# be executing this with a working directory other than the one in which
# the makefile resides.
MAKEFILE_PATH := $(abspath $(firstword $(MAKEFILE_LIST)))

# Creates a local makefile, useful for zipping without getting an absolute path.
Makefile : ${MAKEFILE_PATH}
	ln -s $< $@

${ZIP_NAME} : $(SRCS) $(HEADERS) $(SUBMISSION_INFO_FILE) Makefile
# Although we can update the zip file, this is dangerous since it would
# leave old files in there, and doing so doesn't have any significant performance
# differences.
	[ ! -f $@ ] || rm $@
	$(MKDIR_P) $(dir $@)
	zip $@ $^

submission: $(ZIP_NAME)

-include $(DEPS)

MKDIR_P ?= mkdir -p

.PHONY: clean
.PHONY: submission

-include extra.mk
