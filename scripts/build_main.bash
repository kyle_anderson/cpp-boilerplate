#!/bin/bash
# This must be run with the current directory set to wherever the makefile is.

set -o pipefail -o errexit -o nounset
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

make --jobs "$(nproc)"
