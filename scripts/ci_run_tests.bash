#!/bin/bash
# Runs the unit tests under valgrind, throwing an error exit code if any leaks are detected.
# Only argument, if given, is a file to which the unit test results should be outputed.

set -o pipefail -o errexit -o nounset
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

EXTRA_VALGRIND_OPTS=()
STREAM_REDIRECTS=()
if [[ -n "${1+x}" ]]; then
    EXTRA_VALGRIND_OPTS=('--log-fd=9')
    STREAM_REDIRECTS=("9>&1" ">${1}")
fi

eval valgrind --error-exitcode=1 --leak-check=full --show-leak-kinds=all "${EXTRA_VALGRIND_OPTS[@]}" \
    "${SCRIPT_DIR}"/../build/tests --reporter junit "${STREAM_REDIRECTS[@]}"
